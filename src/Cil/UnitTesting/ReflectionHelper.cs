﻿using Cil.Design;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Cil.UnitTesting {
    internal class ReflectionHelper : IReflectionHelper {
        #region Constructor methods

        /// <summary>
        /// Invokes a costructor of the class with the given name. This class can be private. 
        /// The class must be convertible to the TAbstration type parameter. 
        /// If the constructor of the given class with the given arguments is absent then 
        /// throws a <see cref="MissingConstructorException"/> object.
        /// </summary>
        /// <param name="typeName">The name of the necessary class.</param>
        /// <param name="typeNamespace">The namespace of the necessary class.</param>
        /// <param name="constructorArguments">List of constructor arguments.</param>
        public TAbstraction New<TAbstraction>(
            Type type,
            params object[] constructorArguments
        ) {
            if (type == null)
                throw new ArgumentNullException(nameof(type));
            if (constructorArguments == null)
                throw new ArgumentNullException(nameof(constructorArguments));
            try {
                var result = (TAbstraction)Activator.CreateInstance(
                    type,
                    constructorArguments
                );
                return result;
            } catch (TargetInvocationException tie) {
                // All real exceptions occured at CreateInstance
                // are incapsulated in TargetInvocationException.
                throw tie.InnerException ?? tie;
            } catch (MissingMethodException mme) {
                throw new MissingConstructorException(
                    this._getMissingConstructorMessage(
                        type.Name,
                        nameof(New)
                    ),
                   mme);
            }
        }

        #endregion Constructor methods

        #region Test constructors for null arguments check

        #region Test constructors with default checking functions

        /// <summary>
        /// Tests the constructor of the specified <paramref name="typeName"/> 
        /// in the specified <paramref name="typeNamespace"/> (optional) taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null.
        /// </summary>
        /// <param name="type">The type which constructor is being checked.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        public void TestConstructorForNullArgumentsCheck(
            Type type,
            params object[] constructorArguments
        ) {
            this._checkNotNull(
                type,
                constructorArguments
            );
            var exceptionsAccumulator = new List<Exception>();
            this._testConstructorForNullArgumentsCheck(
                type,
                this._getDefaultExceptionAggregator(exceptionsAccumulator, type.Name),
                this._getDefaultChecker(exceptionsAccumulator, type.Name),
                null,
                constructorArguments
            );
        }

        /// <summary>
        /// Tests the constructor of the specified <paramref name="typeName"/> 
        /// in the specified <paramref name="typeNamespace"/> (optional) taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null. Arguments marked 
        /// with the <see cref="CanBeNullAttribute"/> are not being checked.
        /// </summary>
        /// <param name="type">The type which constructor is being checked.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        public void TestConstructorForNullArgumentsCheckExceptCanBeNulls(
            Type type,
            params object[] constructorArguments
        ) {
            this._checkNotNull(
                type,
                constructorArguments
            );
            this.TestConstructorForNullArgumentsCheckExceptAttributes<CanBeNullAttribute>(
                type,
                constructorArguments
            );
        }

        /// <summary>
        /// Tests the constructor of the specified <paramref name="typeName"/> 
        /// in the specified <paramref name="typeNamespace"/> (optional) taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null. Arguments marked 
        /// with the <typeparamref name="TAttrToSkip"/> are not being checked.
        /// </summary>
        /// <param name="type">The type which constructor is being checked.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        /// <typeparam name="TAttrToSkip">The method will not check constructor parameters 
        /// marked with this attribute.</typeparam>
        public void TestConstructorForNullArgumentsCheckExceptAttributes<TAttrToSkip>(
            Type type,
            params object[] constructorArguments
        )
            where TAttrToSkip : Attribute 
        {
            this._checkNotNull(
                type,
                constructorArguments
            );
            var exceptionsAccumulator = new List<Exception>();
            this._testConstructorForNullArgumentsCheck(
                type,
                this._getDefaultExceptionAggregator(exceptionsAccumulator, type.Name),
                this._getDefaultChecker(exceptionsAccumulator, type.Name),
                typeof(TAttrToSkip),
                constructorArguments
            );
        }

        private Action<Action> _getDefaultExceptionAggregator(
            List<Exception> exceptionsAccumulator,
            string typeName
        ) {
            return (action) => {
                action();
                if (exceptionsAccumulator.Any()) {
                    if (exceptionsAccumulator.Count == 1)
                        throw exceptionsAccumulator.Single();
                    throw new AggregateException(
                        "Multiple exceptions were thrown during the testing \"" +
                        typeName + "\" constructor for null arguments checks.",
                        exceptionsAccumulator.ToArray()
                    );
                }
            };
        }

        private Action<Action, ParameterInfo> _getDefaultChecker(
            List<Exception> exceptionsAccumulator,
            string typeName
        ) {
            return (action, parameterInfo) => {
                var argumentNullException = (ArgumentNullException)null;
                var otherException = (Exception)null;
                try {
                    action();
                } catch (ArgumentNullException e) {
                    argumentNullException = e;
                } catch (Exception e) {
                    otherException = e;
                }
                if (argumentNullException == null) {
                    var message = otherException == null
                        ? "no exception was thrown"
                        : otherException.GetType() + " was thrown";
                    exceptionsAccumulator.Add(new MissingExceptionException(nameof(ArgumentNullException) +
                        " was expected for null value of the parameter \"" +
                        parameterInfo.ParameterType.Name + " " + parameterInfo.Name +
                        "\" of the \"" + typeName + "\" constructor, but " + message + ".",
                        otherException
                    ));
                }
            };
        }

        #endregion Test constructors with default checking functions

        #region Test constructors with custom checking functions

        /// <summary>
        /// Tests the constructor of the specified <paramref name="typeName"/> 
        /// in the specified <paramref name="typeNamespace"/> (optional) taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null.
        /// </summary>
        /// <remarks>In case of NUnit test framework the <paramref name="exceptionsAggregator"/> 
        /// might be <code>a => Assert.Multiple(() => a())</code> while the 
        /// <paramref name="checker"/> might be 
        /// <code>(a, p) => Assert.Throws<ArgumentNullException>(() => a(), p.Name)</code>.</remarks>
        /// <param name="type">The type which constructor is being checked.</param>
        /// <param name="exceptionsAggregator">Function aggregating possible exceptions, see 
        /// the example in the remarks section.</param>
        /// <param name="checker">Function verifying the given code throws the exception, 
        /// see the example in the remarks section.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        public void TestConstructorForNullArgumentsCheck(
            Type type,
            Action<Action> exceptionsAggregator,
            Action<Action, ParameterInfo> checker,
            params object[] constructorArguments
        ) {
            this._checkNotNull(
                type,
                exceptionsAggregator,
                checker,
                constructorArguments
            );
            this._testConstructorForNullArgumentsCheck(
                type,
                exceptionsAggregator,
                checker,
                null,
                constructorArguments
            );
        }

        /// <summary>
        /// Tests the constructor of the specified <paramref name="typeName"/> 
        /// in the specified <paramref name="typeNamespace"/> (optional) taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null. Arguments marked 
        /// with the <see cref="CanBeNullAttribute"/> are not being checked.
        /// </summary>
        /// <remarks>In case of NUnit test framework the <paramref name="exceptionsAggregator"/> 
        /// might be <code>a => Assert.Multiple(() => a())</code> while the 
        /// <paramref name="checker"/> might be 
        /// <code>(a, p) => Assert.Throws<ArgumentNullException>(() => a(), p.Name)</code>.</remarks>
        /// <param name="type">The type which constructor is being checked.</param>
        /// <param name="exceptionsAggregator">Function aggregating possible exceptions, see 
        /// the example in the remarks section.</param>
        /// <param name="checker">Function verifying the given code throws the exception, 
        /// see the example in the remarks section.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        public void TestConstructorForNullArgumentsCheckExceptCanBeNulls(
            Type type,
            Action<Action> exceptionsAggregator,
            Action<Action, ParameterInfo> checker,
            params object[] constructorArguments
        ) {
            this._checkNotNull(
                type,
                exceptionsAggregator,
                checker,
                constructorArguments
            );
            this.TestConstructorForNullArgumentsCheckExceptAttributes<CanBeNullAttribute>(
                type,
                exceptionsAggregator,
                checker,
                constructorArguments
            );
        }

        /// <summary>
        /// Tests the constructor of the specified <paramref name="typeName"/> 
        /// in the specified <paramref name="typeNamespace"/> (optional) taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null. Arguments marked 
        /// with the <typeparamref name="TAttrToSkip"/> are not being checked.
        /// </summary>
        /// <remarks>In case of NUnit test framework the <paramref name="exceptionsAggregator"/> 
        /// might be <code>a => Assert.Multiple(() => a())</code> while the 
        /// <paramref name="checker"/> might be 
        /// <code>(a, p) => Assert.Throws<ArgumentNullException>(() => a(), p.Name)</code>.</remarks>
        /// <param name="type">The type which constructor is being checked.</param>
        /// <param name="exceptionsAggregator">Function aggregating possible exceptions, see 
        /// the example in the remarks section.</param>
        /// <param name="checker">Function verifying the given code throws the exception, 
        /// see the example in the remarks section.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        /// <typeparam name="TAttrToSkip">The method will not check constructor parameters 
        /// marked with this attribute.</typeparam>
        public void TestConstructorForNullArgumentsCheckExceptAttributes<TAttrToSkip>(
            Type type,
            Action<Action> exceptionsAggregator,
            Action<Action, ParameterInfo> checker,
            params object[] constructorArguments
        )
            where TAttrToSkip : Attribute 
        {
            this._checkNotNull(
                type,
                exceptionsAggregator,
                checker,
                constructorArguments
            );
            this._testConstructorForNullArgumentsCheck(
                type,
                exceptionsAggregator,
                checker,
                typeof(TAttrToSkip),
                constructorArguments
            );
        }

        #endregion Test constructors with custom checking functions

        private void _testConstructorForNullArgumentsCheck(
            Type type,
            Action<Action> exceptionsAggregator,
            Action<Action, ParameterInfo> checker,
            [CanBeNull]Type attrToSkip,
            params object[] constructorArguments
        ) {
            var parametersTypes = constructorArguments
                .Select(x => x.GetType())
                .ToArray();
            var ctor = type.GetConstructor(parametersTypes);
            if (ctor == null)
                throw new MissingConstructorException(
                    this._getMissingConstructorMessage(
                        type.Name,
                        nameof(TestConstructorForNullArgumentsCheck)
                    )
                );
            var parametersInfos = ctor.GetParameters();
            exceptionsAggregator(() => {
                for (int i = 0; i < constructorArguments.Length; i++) {
                    var parameterInfo = parametersInfos[i];
                    if (true
                        && attrToSkip != null
                        && parameterInfo.GetCustomAttributes(attrToSkip, true).Any()
                        || parameterInfo.ParameterType.IsValueType
                    )
                        continue;
                    var localParametersValues = new object[constructorArguments.Length];
                    constructorArguments.CopyTo(localParametersValues, 0);
                    localParametersValues[i] = null;
                    checker(
                        () => {
                            try {
                                ctor.Invoke(localParametersValues);
                            } catch (TargetInvocationException e) {
                                // All exceptions occured inside a 
                                // reflection based call are incapsulated 
                                // in TargetInvocationException. Here 
                                // we should "unbox" and rethrow the 
                                // real exception.
                                throw e.InnerException;
                            }
                        },
                        parameterInfo
                    );
                }
            });
        }

        #endregion Test constructors for null arguments check

        #region Infrastructure

        private void _checkNotNull(
            Type type,
            Action<Action> exceptionsAggregator,
            Action<Action, ParameterInfo> checker,
            object[] constructorArguments
        ) {
            this._checkNotNull(type, constructorArguments);
            if (type == null)
                throw new ArgumentNullException(nameof(Type));
            if (constructorArguments == null)
                throw new ArgumentNullException(nameof(constructorArguments));
        }

        private void _checkNotNull(
            Type type,
            object[] constructorArguments
        ) {
            if (type == null)
                throw new ArgumentNullException(nameof(Type));
            if (constructorArguments == null)
                throw new ArgumentNullException(nameof(constructorArguments));
        }

        private string _getMissingConstructorMessage(
            string missingConstructorType,
            string methodWithException
        ) {
            return "Cannot find a constructor for the type \"" +
               $"{missingConstructorType}\" satisfying the given arguments. " +
                "It seems that the constructor signature has been changed " +
                "(or the type has been moved to another assembly or namespace) " +
                "and this change is not considered in the code " +
               $"calling that constructor via reflection (the \"{methodWithException}\" " +
               $"method of the \"{nameof(UnitTestsHelper)}\" class).";
        }

        #endregion Infrastructure
    }
}
