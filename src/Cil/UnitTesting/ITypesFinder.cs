﻿using System;

namespace Cil.UnitTesting {
    internal interface ITypesFinder {
        Type FindType(
            string typeName,
            string typeNamespace = null
        );
    }
}
