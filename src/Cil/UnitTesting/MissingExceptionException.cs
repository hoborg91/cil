﻿using System;

namespace Cil.UnitTesting {
    /// <summary>
    /// This exception is thrown when some code has not thrown 
    /// the expected exception.
    /// </summary>
    [Obsolete("The Cil package has been refactored and renamed to Jail.")]
    public class MissingExceptionException : Exception {
        /// <summary>
        /// This exception is thrown when some code has not thrown 
        /// the expected exception.
        /// </summary>
        public MissingExceptionException(
            string message, 
            Exception innerException
        ) : base(
            message, 
            innerException
        ) {
        }
    }
}
