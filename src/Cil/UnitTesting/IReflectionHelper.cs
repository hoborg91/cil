﻿using System;
using System.Reflection;

namespace Cil.UnitTesting {
    internal interface IReflectionHelper {
        TAbstraction New<TAbstraction>(Type type, params object[] constructorArguments);
        void TestConstructorForNullArgumentsCheck(Type type, Action<Action> exceptionsAggregator, Action<Action, ParameterInfo> checker, params object[] constructorArguments);
        void TestConstructorForNullArgumentsCheck(Type type, params object[] constructorArguments);
        void TestConstructorForNullArgumentsCheckExceptAttributes<TAttrToSkip>(Type type, Action<Action> exceptionsAggregator, Action<Action, ParameterInfo> checker, params object[] constructorArguments) where TAttrToSkip : Attribute;
        void TestConstructorForNullArgumentsCheckExceptAttributes<TAttrToSkip>(Type type, params object[] constructorArguments) where TAttrToSkip : Attribute;
        void TestConstructorForNullArgumentsCheckExceptCanBeNulls(Type type, Action<Action> exceptionsAggregator, Action<Action, ParameterInfo> checker, params object[] constructorArguments);
        void TestConstructorForNullArgumentsCheckExceptCanBeNulls(Type type, params object[] constructorArguments);
    }
}