﻿using Cil.Design;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace Cil.UnitTesting {
    /// <summary>
    /// A helper class which can invoke constructors of the private classes.
    /// </summary>
    /// <remarks>How to use. Suppose you want to get an instance of the private 
    /// class TPrivate located inside the public class TPublic in the external 
    /// assembly. Then you shoud write the following. 
    /// var p = new UnitTestsHelper(typeof(TPublic).Assembly).New&lt;object&gt;("TPrivate");
    /// </remarks>
    [Obsolete("The Cil package has been refactored and renamed to Jail.")]
    public class UnitTestsHelper {
        private readonly ITypesFinder _typesFinder;

        private readonly IReflectionHelper _reflectionHelper;

        /// <summary>
        /// A helper class which can invoke constructors of the private classes.
        /// </summary>
        /// <remarks>How to use. Suppose you want to get an instance of the private 
        /// class TPrivate located inside the public class TPublic in the external 
        /// assembly. Then you shoud write the following. 
        /// var p = new UnitTestsHelper(typeof(TPublic).Assembly).New&lt;object&gt;("TPrivate");
        /// </remarks>
        /// <param name="assembly">The assembly containing the necessary class.</param>
        public UnitTestsHelper(Assembly assembly) {
            if (assembly == null)
                throw new ArgumentNullException(nameof(assembly));
            this._typesFinder = new TypesFinder(assembly);
            this._reflectionHelper = new ReflectionHelper();
        }

        /// <summary>
        /// A helper class which can invoke constructors of the private classes.
        /// </summary>
        /// <remarks>Constructor for unit testing.</remarks>
        internal UnitTestsHelper(
            ITypesFinder typesFinder,
            IReflectionHelper reflectionHelper
        ) {
            this._typesFinder = typesFinder ??
                throw new ArgumentNullException(nameof(typesFinder));
            this._reflectionHelper = reflectionHelper ??
                throw new ArgumentNullException(nameof(reflectionHelper));
        }

        #region Constructor methods

        /// <summary>
        /// Invokes a costructor of the class with the given name. This class can be private. 
        /// The class must be convertible to the TAbstration type parameter. 
        /// If the constructor of the given class with the given arguments is absent then 
        /// throws a <see cref="MissingConstructorException"/> object.
        /// </summary>
        /// <param name="typeName">The name of the necessary class.</param>
        /// <param name="constructorArguments">List of constructor arguments.</param>
        public TAbstraction New<TAbstraction>(
            string typeName,
            params object[] constructorArguments
        ) {
            return this._new<TAbstraction>(
                typeName, 
                null, 
                constructorArguments
            );
        }

        /// <summary>
        /// Invokes a costructor of the class with the given name. This class can be private. 
        /// The class must be convertible to the TAbstration type parameter. 
        /// If the constructor of the given class with the given arguments is absent then 
        /// throws a <see cref="MissingConstructorException"/> object.
        /// </summary>
        /// <param name="typeName">The name of the necessary class.</param>
        /// <param name="typeNamespace">The namespace of the necessary class.</param>
        /// <param name="constructorArguments">List of constructor arguments.</param>
        public TAbstraction New<TAbstraction>(
            string typeName,
            string typeNamespace,
            params object[] constructorArguments
        ) {
            if (typeName == null)
                throw new ArgumentNullException(nameof(typeName));
            if (typeNamespace == null)
                throw new ArgumentNullException(nameof(typeNamespace));
            if (constructorArguments == null)
                throw new ArgumentNullException(nameof(constructorArguments));
            return this._new<TAbstraction>(
                typeName, 
                typeNamespace, 
                constructorArguments
            );
        }

        /// <summary>
        /// Invokes a costructor of the class with the given name. This class can be private. 
        /// The class must be convertible to the TAbstration type parameter. 
        /// If the constructor of the given class with the given arguments is absent then 
        /// throws a <see cref="MissingConstructorException"/> object.
        /// </summary>
        /// <param name="typeName">The name of the necessary class.</param>
        /// <param name="typeNamespace">The namespace of the necessary class.</param>
        /// <param name="constructorArguments">List of constructor arguments.</param>
        private TAbstraction _new<TAbstraction>(
            string typeName,
            string typeNamespace,
            params object[] constructorArguments
        ) {
            var type = this._typesFinder.FindType(typeName, typeNamespace);
            return this._reflectionHelper.New<TAbstraction>(type, constructorArguments);
        }

        #endregion Constructor methods

        #region Test constructors for null arguments check

        #region Test constructors with default checking functions

        #region With namespace specification

        /// <summary>
        /// Tests the constructor of the specified <paramref name="typeName"/> 
        /// in the specified <paramref name="typeNamespace"/> taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null.
        /// </summary>
        /// <param name="typeName">The type which constructor is being checked.</param>
        /// <param name="typeNamespace">The namespace containing the type being checked.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        public void TestConstructorForNullArgumentsCheck(
            string typeName,
            string typeNamespace,
            params object[] constructorArguments
        ) {
            this._checkNotNull(typeName, typeNamespace, constructorArguments);
            var type = this._typesFinder.FindType(typeName, typeNamespace);
            this._reflectionHelper.TestConstructorForNullArgumentsCheck(
                type,
                constructorArguments
            );
        }

        /// <summary>
        /// Tests the constructor of the specified <paramref name="typeName"/> 
        /// in the specified <paramref name="typeNamespace"/> taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null. Arguments marked 
        /// with the <see cref="CanBeNullAttribute"/> are not being checked.
        /// </summary>
        /// <param name="typeName">The type which constructor is being checked.</param>
        /// <param name="typeNamespace">The namespace containing the type being checked.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        public void TestConstructorForNullArgumentsCheckExceptCanBeNulls(
            string typeName,
            string typeNamespace,
            params object[] constructorArguments
        ) {
            this._checkNotNull(typeName, typeNamespace, constructorArguments);
            var type = this._typesFinder.FindType(typeName, typeNamespace);
            this._reflectionHelper.TestConstructorForNullArgumentsCheckExceptCanBeNulls(
                type,
                constructorArguments
            );
        }

        /// <summary>
        /// Tests the constructor of the specified <paramref name="typeName"/> 
        /// in the specified <paramref name="typeNamespace"/> taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null. Arguments marked 
        /// with the <typeparamref name="TAttrToSkip"/> are not being checked.
        /// </summary>
        /// <param name="typeName">The type which constructor is being checked.</param>
        /// <param name="typeNamespace">The namespace containing the type being checked.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        /// <typeparam name="TAttrToSkip">The method will not check constructor parameters 
        /// marked with this attribute.</typeparam>
        public void TestConstructorForNullArgumentsCheckExceptAttributes<TAttrToSkip>(
            string typeName,
            string typeNamespace,
            params object[] constructorArguments
        )
            where TAttrToSkip : Attribute 
        {
            this._checkNotNull(typeName, typeNamespace, constructorArguments);
            var type = this._typesFinder.FindType(typeName, typeNamespace);
            this._reflectionHelper.TestConstructorForNullArgumentsCheckExceptAttributes<TAttrToSkip>(
                type,
                constructorArguments
            );
        }

        #endregion With namespace specification

        #region Without namespace specification

        /// <summary>
        /// Tests the constructor of the specified <paramref name="typeName"/> taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null.
        /// </summary>
        /// <param name="typeName">The type which constructor is being checked.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        public void TestConstructorForNullArgumentsCheck(
            string typeName,
            params object[] constructorArguments
        ) {
            this._checkNotNull(typeName, constructorArguments);
            var type = this._typesFinder.FindType(typeName);
            this._reflectionHelper.TestConstructorForNullArgumentsCheck(
                type,
                constructorArguments
            );
        }

        /// <summary>
        /// Tests the constructor of the specified <paramref name="typeName"/> taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null. Arguments marked 
        /// with the <see cref="CanBeNullAttribute"/> are not being checked.
        /// </summary>
        /// <param name="typeName">The type which constructor is being checked.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        public void TestConstructorForNullArgumentsCheckExceptCanBeNulls(
            string typeName,
            params object[] constructorArguments
        ) {
            this._checkNotNull(typeName, constructorArguments);
            var type = this._typesFinder.FindType(typeName);
            this._reflectionHelper.TestConstructorForNullArgumentsCheckExceptCanBeNulls(
                type,
                constructorArguments
            );
        }

        /// <summary>
        /// Tests the constructor of the specified <paramref name="typeName"/> taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null. Arguments marked 
        /// with the <typeparamref name="TAttrToSkip"/> are not being checked.
        /// </summary>
        /// <param name="typeName">The type which constructor is being checked.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        /// <typeparam name="TAttrToSkip">The method will not check constructor parameters 
        /// marked with this attribute.</typeparam>
        public void TestConstructorForNullArgumentsCheckExceptAttributes<TAttrToSkip>(
            string typeName,
            params object[] constructorArguments
        )
            where TAttrToSkip : Attribute 
        {
            this._checkNotNull(typeName, constructorArguments);
            var type = this._typesFinder.FindType(typeName);
            this._reflectionHelper.TestConstructorForNullArgumentsCheckExceptAttributes<TAttrToSkip>(
                type,
                constructorArguments
            );
        }

        #endregion Without namespace specification

        #region Generics

        /// <summary>
        /// Tests the constructor of the specified type <typeparamref name="T"/> taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null.
        /// </summary>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        /// <typeparam name="T">The type which constructor is being checked.</typeparam>
        public void TestConstructorForNullArgumentsCheck<T>(
            params object[] constructorArguments
        ) {
            if (constructorArguments == null)
                throw new ArgumentNullException(nameof(constructorArguments));
            var type = typeof(T);
            this._reflectionHelper.TestConstructorForNullArgumentsCheck(
                type,
                constructorArguments
            );
        }

        /// <summary>
        /// Tests the constructor of the specified type <typeparamref name="T"/> taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null. Arguments marked 
        /// with the <see cref="CanBeNullAttribute"/> are not being checked.
        /// </summary>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        /// <typeparam name="T">The type which constructor is being checked.</typeparam>
        public void TestConstructorForNullArgumentsCheckExceptCanBeNulls<T>(
            params object[] constructorArguments
        ) {
            if (constructorArguments == null)
                throw new ArgumentNullException(nameof(constructorArguments));
            var type = typeof(T);
            this._reflectionHelper.TestConstructorForNullArgumentsCheckExceptCanBeNulls(
                type,
                constructorArguments
            );
        }

        /// <summary>
        /// Tests the constructor of the specified type <typeparamref name="T"/> taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null. Arguments marked 
        /// with the <typeparamref name="TAttrToSkip"/> are not being checked.
        /// </summary>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        /// <typeparam name="T">The type which constructor is being checked.</typeparam>
        /// <typeparam name="TAttrToSkip">The method will not check constructor parameters 
        /// marked with this attribute.</typeparam>
        public void TestConstructorForNullArgumentsCheckExceptAttributes<T, TAttrToSkip>(
            params object[] constructorArguments
        )
            where TAttrToSkip : Attribute {
            if (constructorArguments == null)
                throw new ArgumentNullException(nameof(constructorArguments));
            var type = typeof(T);
            this._reflectionHelper.TestConstructorForNullArgumentsCheckExceptAttributes<TAttrToSkip>(
                type,
                constructorArguments
            );
        }

        #endregion Generics

        #region Expressions

        /// <summary>
        /// Tests the constructor (specified in the <paramref name="constructorCall"/>) 
        /// of the specified type <typeparamref name="T"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null.
        /// </summary>
        /// <param name="constructorCall">Expression representing a constructor call.</param>
        /// <typeparam name="T">The type which constructor is being checked.</typeparam>
        public void TestConstructorForNullArgumentsCheck<T>(
            Expression<Func<T>> constructorCall
        ) {
            var constructorArguments = this._checkAndGetConstructorArguments(constructorCall);
            var type = typeof(T);
            this._reflectionHelper.TestConstructorForNullArgumentsCheck(
                type,
                constructorArguments
            );
        }

        /// <summary>
        /// Tests the constructor (specified in the <paramref name="constructorCall"/>) 
        /// of the specified type <typeparamref name="T"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null. Arguments marked 
        /// with the <see cref="CanBeNullAttribute"/> are not being checked.
        /// </summary>
        /// <param name="constructorCall">Expression representing a constructor call.</param>
        /// <typeparam name="T">The type which constructor is being checked.</typeparam>
        public void TestConstructorForNullArgumentsCheckExceptCanBeNulls<T>(
            Expression<Func<T>> constructorCall
        ) {
            var constructorArguments = this._checkAndGetConstructorArguments(constructorCall);
            var type = typeof(T);
            this._reflectionHelper.TestConstructorForNullArgumentsCheckExceptCanBeNulls(
                type,
                constructorArguments
            );
        }

        /// <summary>
        /// Tests the constructor (specified in the <paramref name="constructorCall"/>) 
        /// of the specified type <typeparamref name="T"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null. Arguments marked 
        /// with the <typeparamref name="TAttrToSkip"/> are not being checked.
        /// </summary>
        /// <param name="constructorCall">Expression representing a constructor call.</param>
        /// <typeparam name="T">The type which constructor is being checked.</typeparam>
        /// <typeparam name="TAttrToSkip">The method will not check constructor parameters 
        /// marked with this attribute.</typeparam>
        public void TestConstructorForNullArgumentsCheckExceptAttributes<T, TAttrToSkip>(
            Expression<Func<T>> constructorCall
        )
            where TAttrToSkip : Attribute 
        {
            var constructorArguments = this._checkAndGetConstructorArguments(constructorCall);
            var type = typeof(T);
            this._reflectionHelper.TestConstructorForNullArgumentsCheckExceptAttributes<TAttrToSkip>(
                type,
                constructorArguments
            );
        }

        #endregion Expressions

        #endregion Test constructors with default checking functions

        #region Test constructors with custom checking functions

        #region With namespace specification

        /// <summary>
        /// Tests the constructor of the specified <paramref name="typeName"/> 
        /// in the specified <paramref name="typeNamespace"/> taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null.
        /// </summary>
        /// <remarks>In case of NUnit test framework the <paramref name="exceptionsAggregator"/> 
        /// might be <code>a => Assert.Multiple(() => a())</code> while the 
        /// <paramref name="checker"/> might be 
        /// <code>(a, p) => Assert.Throws<ArgumentNullException>(() => a(), p.Name)</code>.</remarks>
        /// <param name="typeName">The type which constructor is being checked.</param>
        /// <param name="typeNamespace">The namespace containing the type being checked.</param>
        /// <param name="exceptionsAggregator">Function aggregating possible exceptions, see 
        /// the example in the remarks section.</param>
        /// <param name="checker">Function verifying the given code throws the exception, 
        /// see the example in the remarks section.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        public void TestConstructorForNullArgumentsCheck(
            string typeName,
            string typeNamespace,
            Action<Action> exceptionsAggregator,
            Action<Action, ParameterInfo> checker,
            params object[] constructorArguments
        ) {
            this._checkNotNull(typeName, typeNamespace, constructorArguments);
            var type = this._typesFinder.FindType(typeName);
            this._reflectionHelper.TestConstructorForNullArgumentsCheck(
                type,
                exceptionsAggregator,
                checker,
                constructorArguments
            );
        }

        /// <summary>
        /// Tests the constructor of the specified <paramref name="typeName"/> 
        /// in the specified <paramref name="typeNamespace"/> taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null. Arguments marked 
        /// with the <see cref="CanBeNullAttribute"/> are not being checked.
        /// </summary>
        /// <remarks>In case of NUnit test framework the <paramref name="exceptionsAggregator"/> 
        /// might be <code>a => Assert.Multiple(() => a())</code> while the 
        /// <paramref name="checker"/> might be 
        /// <code>(a, p) => Assert.Throws<ArgumentNullException>(() => a(), p.Name)</code>.</remarks>
        /// <param name="typeName">The type which constructor is being checked.</param>
        /// <param name="typeNamespace">The namespace containing the type being checked.</param>
        /// <param name="exceptionsAggregator">Function aggregating possible exceptions, see 
        /// the example in the remarks section.</param>
        /// <param name="checker">Function verifying the given code throws the exception, 
        /// see the example in the remarks section.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        public void TestConstructorForNullArgumentsCheckExceptCanBeNulls(
            string typeName,
            string typeNamespace,
            Action<Action> exceptionsAggregator,
            Action<Action, ParameterInfo> checker,
            params object[] constructorArguments
        ) {
            this._checkNotNull(typeName, typeNamespace, constructorArguments);
            var type = this._typesFinder.FindType(typeName);
            this._reflectionHelper.TestConstructorForNullArgumentsCheckExceptCanBeNulls(
                type,
                exceptionsAggregator,
                checker,
                constructorArguments
            );
        }

        /// <summary>
        /// Tests the constructor of the specified <paramref name="typeName"/> 
        /// in the specified <paramref name="typeNamespace"/> taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null. Arguments marked 
        /// with the <typeparamref name="TAttrToSkip"/> are not being checked.
        /// </summary>
        /// <remarks>In case of NUnit test framework the <paramref name="exceptionsAggregator"/> 
        /// might be <code>a => Assert.Multiple(() => a())</code> while the 
        /// <paramref name="checker"/> might be 
        /// <code>(a, p) => Assert.Throws<ArgumentNullException>(() => a(), p.Name)</code>.</remarks>
        /// <param name="typeName">The type which constructor is being checked.</param>
        /// <param name="typeNamespace">The namespace containing the type being checked.</param>
        /// <param name="exceptionsAggregator">Function aggregating possible exceptions, see 
        /// the example in the remarks section.</param>
        /// <param name="checker">Function verifying the given code throws the exception, 
        /// see the example in the remarks section.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        /// <typeparam name="TAttrToSkip">The method will not check constructor parameters 
        /// marked with this attribute.</typeparam>
        public void TestConstructorForNullArgumentsCheckExceptAttributes<TAttrToSkip>(
            string typeName,
            string typeNamespace,
            Action<Action> exceptionsAggregator,
            Action<Action, ParameterInfo> checker,
            params object[] constructorArguments
        ) 
            where TAttrToSkip : Attribute 
        {
            this._checkNotNull(typeName, typeNamespace, constructorArguments);
            var type = this._typesFinder.FindType(typeName);
            this._reflectionHelper.TestConstructorForNullArgumentsCheckExceptAttributes<TAttrToSkip>(
                type,
                exceptionsAggregator,
                checker,
                constructorArguments
            );
        }

        #endregion With namespace specification

        #region Without namespace specification

        /// <summary>
        /// Tests the constructor of the specified <paramref name="typeName"/> taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null.
        /// </summary>
        /// <remarks>In case of NUnit test framework the <paramref name="exceptionsAggregator"/> 
        /// might be <code>a => Assert.Multiple(() => a())</code> while the 
        /// <paramref name="checker"/> might be 
        /// <code>(a, p) => Assert.Throws<ArgumentNullException>(() => a(), p.Name)</code>.</remarks>
        /// <param name="typeName">The type which constructor is being checked.</param>
        /// <param name="exceptionsAggregator">Function aggregating possible exceptions, see 
        /// the example in the remarks section.</param>
        /// <param name="checker">Function verifying the given code throws the exception, 
        /// see the example in the remarks section.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        public void TestConstructorForNullArgumentsCheck(
            string typeName,
            Action<Action> exceptionsAggregator,
            Action<Action, ParameterInfo> checker,
            params object[] constructorArguments
        ) {
            this._checkNotNull(typeName, constructorArguments);
            var type = this._typesFinder.FindType(typeName);
            this._reflectionHelper.TestConstructorForNullArgumentsCheck(
                type,
                exceptionsAggregator,
                checker,
                constructorArguments
            );
        }

        /// <summary>
        /// Tests the constructor of the specified <paramref name="typeName"/> taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null. Arguments marked 
        /// with the <see cref="CanBeNullAttribute"/> are not being checked.
        /// </summary>
        /// <remarks>In case of NUnit test framework the <paramref name="exceptionsAggregator"/> 
        /// might be <code>a => Assert.Multiple(() => a())</code> while the 
        /// <paramref name="checker"/> might be 
        /// <code>(a, p) => Assert.Throws<ArgumentNullException>(() => a(), p.Name)</code>.</remarks>
        /// <param name="typeName">The type which constructor is being checked.</param>
        /// <param name="exceptionsAggregator">Function aggregating possible exceptions, see 
        /// the example in the remarks section.</param>
        /// <param name="checker">Function verifying the given code throws the exception, 
        /// see the example in the remarks section.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        public void TestConstructorForNullArgumentsCheckExceptCanBeNulls(
            string typeName,
            Action<Action> exceptionsAggregator,
            Action<Action, ParameterInfo> checker,
            params object[] constructorArguments
        ) {
            this._checkNotNull(typeName, constructorArguments);
            var type = this._typesFinder.FindType(typeName);
            this._reflectionHelper.TestConstructorForNullArgumentsCheckExceptCanBeNulls(
                type,
                exceptionsAggregator,
                checker,
                constructorArguments
            );
        }

        /// <summary>
        /// Tests the constructor of the specified <paramref name="typeName"/> taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null. Arguments marked 
        /// with the <typeparamref name="TAttrToSkip"/> are not being checked.
        /// </summary>
        /// <remarks>In case of NUnit test framework the <paramref name="exceptionsAggregator"/> 
        /// might be <code>a => Assert.Multiple(() => a())</code> while the 
        /// <paramref name="checker"/> might be 
        /// <code>(a, p) => Assert.Throws<ArgumentNullException>(() => a(), p.Name)</code>.</remarks>
        /// <param name="typeName">The type which constructor is being checked.</param>
        /// <param name="exceptionsAggregator">Function aggregating possible exceptions, see 
        /// the example in the remarks section.</param>
        /// <param name="checker">Function verifying the given code throws the exception, 
        /// see the example in the remarks section.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        /// <typeparam name="TAttrToSkip">The method will not check constructor parameters 
        /// marked with this attribute.</typeparam>
        public void TestConstructorForNullArgumentsCheckExceptAttributes<TAttrToSkip>(
            string typeName,
            Action<Action> exceptionsAggregator,
            Action<Action, ParameterInfo> checker,
            params object[] constructorArguments
        )
            where TAttrToSkip : Attribute 
        {
            this._checkNotNull(typeName, constructorArguments);
            var type = this._typesFinder.FindType(typeName);
            this._reflectionHelper.TestConstructorForNullArgumentsCheckExceptAttributes<TAttrToSkip>(
                type,
                exceptionsAggregator,
                checker,
                constructorArguments
            );
        }

        #endregion Without namespace specification

        #region Generics

        /// <summary>
        /// Tests the constructor of the specified type <typeparamref name="T"/> taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null.
        /// </summary>
        /// <remarks>In case of NUnit test framework the <paramref name="exceptionsAggregator"/> 
        /// might be <code>a => Assert.Multiple(() => a())</code> while the 
        /// <paramref name="checker"/> might be 
        /// <code>(a, p) => Assert.Throws<ArgumentNullException>(() => a(), p.Name)</code>.</remarks>
        /// <param name="exceptionsAggregator">Function aggregating possible exceptions, see 
        /// the example in the remarks section.</param>
        /// <param name="checker">Function verifying the given code throws the exception, 
        /// see the example in the remarks section.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        /// <typeparamref name="T">The type which constructor is being checked.</typeparamref>
        public void TestConstructorForNullArgumentsCheck<T>(
            Action<Action> exceptionsAggregator,
            Action<Action, ParameterInfo> checker,
            params object[] constructorArguments
        ) {
            if (constructorArguments == null)
                throw new ArgumentNullException(nameof(constructorArguments));
            var type = typeof(T);
            this._reflectionHelper.TestConstructorForNullArgumentsCheck(
                type,
                exceptionsAggregator,
                checker,
                constructorArguments
            );
        }

        /// <summary>
        /// Tests the constructor of the specified type <typeparamref name="T"/> taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null. Arguments marked 
        /// with the <see cref="CanBeNullAttribute"/> are not being checked.
        /// </summary>
        /// <remarks>In case of NUnit test framework the <paramref name="exceptionsAggregator"/> 
        /// might be <code>a => Assert.Multiple(() => a())</code> while the 
        /// <paramref name="checker"/> might be 
        /// <code>(a, p) => Assert.Throws<ArgumentNullException>(() => a(), p.Name)</code>.</remarks>
        /// <param name="exceptionsAggregator">Function aggregating possible exceptions, see 
        /// the example in the remarks section.</param>
        /// <param name="checker">Function verifying the given code throws the exception, 
        /// see the example in the remarks section.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        /// <typeparamref name="T">The type which constructor is being checked.</typeparamref>
        public void TestConstructorForNullArgumentsCheckExceptCanBeNulls<T>(
            Action<Action> exceptionsAggregator,
            Action<Action, ParameterInfo> checker,
            params object[] constructorArguments
        ) {
            if (constructorArguments == null)
                throw new ArgumentNullException(nameof(constructorArguments));
            var type = typeof(T);
            this._reflectionHelper.TestConstructorForNullArgumentsCheckExceptCanBeNulls(
                type,
                exceptionsAggregator,
                checker,
                constructorArguments
            );
        }

        /// <summary>
        /// Tests the constructor of the specified type <typeparamref name="T"/> taking 
        /// the specified <paramref name="constructorArguments"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null. Arguments marked 
        /// with the <typeparamref name="TAttrToSkip"/> are not being checked.
        /// </summary>
        /// <remarks>In case of NUnit test framework the <paramref name="exceptionsAggregator"/> 
        /// might be <code>a => Assert.Multiple(() => a())</code> while the 
        /// <paramref name="checker"/> might be 
        /// <code>(a, p) => Assert.Throws<ArgumentNullException>(() => a(), p.Name)</code>.</remarks>
        /// <param name="exceptionsAggregator">Function aggregating possible exceptions, see 
        /// the example in the remarks section.</param>
        /// <param name="checker">Function verifying the given code throws the exception, 
        /// see the example in the remarks section.</param>
        /// <param name="constructorArguments">Valid arguments for the constructor being checked.</param>
        /// <typeparamref name="T">The type which constructor is being checked.</typeparamref>
        /// <typeparam name="TAttrToSkip">The method will not check constructor parameters 
        /// marked with this attribute.</typeparam>
        public void TestConstructorForNullArgumentsCheckExceptAttributes<T, TAttrToSkip>(
            Action<Action> exceptionsAggregator,
            Action<Action, ParameterInfo> checker,
            params object[] constructorArguments
        )
            where TAttrToSkip : Attribute 
        {
            if (constructorArguments == null)
                throw new ArgumentNullException(nameof(constructorArguments));
            var type = typeof(T);
            this._reflectionHelper.TestConstructorForNullArgumentsCheckExceptAttributes<TAttrToSkip>(
                type,
                exceptionsAggregator,
                checker,
                constructorArguments
            );
        }

        #endregion Generics

        #region Expressions

        /// <summary>
        /// Tests the constructor (specified in the <paramref name="constructorCall"/>) 
        /// of the specified type <typeparamref name="T"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null.
        /// </summary>
        /// <remarks>In case of NUnit test framework the <paramref name="exceptionsAggregator"/> 
        /// might be <code>a => Assert.Multiple(() => a())</code> while the 
        /// <paramref name="checker"/> might be 
        /// <code>(a, p) => Assert.Throws<ArgumentNullException>(() => a(), p.Name)</code>.</remarks>
        /// <param name="constructorCall">Expression representing a constructor call.</param>
        /// <param name="exceptionsAggregator">Function aggregating possible exceptions, see 
        /// the example in the remarks section.</param>
        /// <param name="checker">Function verifying the given code throws the exception, 
        /// see the example in the remarks section.</param>
        /// <typeparamref name="T">The type which constructor is being checked.</typeparamref>
        public void TestConstructorForNullArgumentsCheck<T>(
            Expression<Func<T>> constructorCall,
            Action<Action> exceptionsAggregator,
            Action<Action, ParameterInfo> checker
        ) {
            var constructorArguments = this._checkAndGetConstructorArguments(constructorCall);
            var type = typeof(T);
            this._reflectionHelper.TestConstructorForNullArgumentsCheck(
                type,
                exceptionsAggregator,
                checker,
                constructorArguments
            );
        }

        /// <summary>
        /// Tests the constructor (specified in the <paramref name="constructorCall"/>) 
        /// of the specified type <typeparamref name="T"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null. Arguments marked 
        /// with the <see cref="CanBeNullAttribute"/> are not being checked.
        /// </summary>
        /// <remarks>In case of NUnit test framework the <paramref name="exceptionsAggregator"/> 
        /// might be <code>a => Assert.Multiple(() => a())</code> while the 
        /// <paramref name="checker"/> might be 
        /// <code>(a, p) => Assert.Throws<ArgumentNullException>(() => a(), p.Name)</code>.</remarks>
        /// <param name="constructorCall">Expression representing a constructor call.</param>
        /// <param name="exceptionsAggregator">Function aggregating possible exceptions, see 
        /// the example in the remarks section.</param>
        /// <param name="checker">Function verifying the given code throws the exception, 
        /// see the example in the remarks section.</param>
        /// <typeparamref name="T">The type which constructor is being checked.</typeparamref>
        public void TestConstructorForNullArgumentsCheckExceptCanBeNulls<T>(
            Expression<Func<T>> constructorCall,
            Action<Action> exceptionsAggregator,
            Action<Action, ParameterInfo> checker
        ) {
            var constructorArguments = this._checkAndGetConstructorArguments(constructorCall);
            var type = typeof(T);
            this._reflectionHelper.TestConstructorForNullArgumentsCheckExceptCanBeNulls(
                type,
                exceptionsAggregator,
                checker,
                constructorArguments
            );
        }

        /// <summary>
        /// Tests the constructor (specified in the <paramref name="constructorCall"/>) 
        /// of the specified type <typeparamref name="T"/>. Fails iff 
        /// the constructor does not throw <see cref="ArgumentNullException"/> 
        /// when any of the reference type parameter is null. Arguments marked 
        /// with the <typeparamref name="TAttrToSkip"/> are not being checked.
        /// </summary>
        /// <remarks>In case of NUnit test framework the <paramref name="exceptionsAggregator"/> 
        /// might be <code>a => Assert.Multiple(() => a())</code> while the 
        /// <paramref name="checker"/> might be 
        /// <code>(a, p) => Assert.Throws<ArgumentNullException>(() => a(), p.Name)</code>.</remarks>
        /// <param name="constructorCall">Expression representing a constructor call.</param>
        /// <param name="exceptionsAggregator">Function aggregating possible exceptions, see 
        /// the example in the remarks section.</param>
        /// <param name="checker">Function verifying the given code throws the exception, 
        /// see the example in the remarks section.</param>
        /// <typeparamref name="T">The type which constructor is being checked.</typeparamref>
        /// <typeparam name="TAttrToSkip">The method will not check constructor parameters 
        /// marked with this attribute.</typeparam>
        public void TestConstructorForNullArgumentsCheckExceptAttributes<T, TAttrToSkip>(
            Expression<Func<T>> constructorCall,
            Action<Action> exceptionsAggregator,
            Action<Action, ParameterInfo> checker
        )
            where TAttrToSkip : Attribute 
        {
            var constructorArguments = this._checkAndGetConstructorArguments(constructorCall);
            var type = typeof(T);
            this._reflectionHelper.TestConstructorForNullArgumentsCheckExceptAttributes<TAttrToSkip>(
                type,
                exceptionsAggregator,
                checker,
                constructorArguments
            );
        }

        #endregion Expressions

        #endregion Test constructors with custom checking functions

        #region Infrastructure

        private object[] _checkAndGetConstructorArguments<T>(
            Expression<Func<T>> constructorCall
        ) {
            if (constructorCall == null)
                throw new ArgumentNullException(nameof(constructorCall));
            var newExpression = constructorCall.Body as NewExpression;
            if (newExpression == null)
                throw new ArgumentException("The given expression is " +
                    "expected to be an initialization, such as () => " +
                    "new SomeClass(argumentsList).");
            var args = newExpression.Arguments;
            var constructorArguments = new List<object>();
            foreach (var argExpression in args) {
                var smth = Expression.Lambda<Func<object>>(
                    Expression.Convert(argExpression, typeof(object))
                );
                object value;
                try {
                    var compiled = smth.Compile();
                    value = compiled();
                } catch(Exception ex) {
                    throw new Exception("Exception occured while calculating " +
                        "one of the constructor arguments. Inspect the inner " +
                        "exception for additional information. To localize the " +
                        "error try to supply only calculated values to the " +
                        "Expression representing the constructor call.", ex);
                }
                constructorArguments.Add(value);
            }
            return constructorArguments.ToArray();
        }

        private void _checkNotNull(
            string typeName,
            string typeNamespace,
            params object[] constructorArguments
        ) {
            this._checkNotNull(typeName, constructorArguments);
            if (typeNamespace == null)
                throw new ArgumentNullException(nameof(typeNamespace));
        }

        private void _checkNotNull(
            string typeName,
            params object[] constructorArguments
        ) {
            if (typeName == null)
                throw new ArgumentNullException(nameof(typeName));
            if (constructorArguments == null)
                throw new ArgumentNullException(nameof(constructorArguments));
        }

        #endregion Infrastructure

        #endregion Test constructors for null arguments check
    }

    /// <summary>
    /// A helper class which can invoke constructors of the private classes.
    /// </summary>
    /// <remarks>How to use. Suppose you want to get an instance of the private 
    /// class TPrivate located inside the public class TPublic in the external 
    /// assembly. Then you shoud write the following. 
    /// var p = new UnitTestsHelper&lt;TPublic&gt;().New&lt;object&gt;("TPrivate");
    /// </remarks>
    /// <typeparam name="T">Any visible type from the assembly you whant to inspect.</typeparam>
    public class UnitTestsHelper<T> : UnitTestsHelper {
        /// <summary>
        /// A helper class which can invoke constructors of the private classes.
        /// </summary>
        /// <remarks>How to use. Suppose you want to get an instance of the private 
        /// class TPrivate located inside the public class TPublic in the external 
        /// assembly. Then you shoud write the following. 
        /// var p = new UnitTestsHelper&lt;TPublic&gt;().New&lt;object&gt;("TPrivate");
        /// </remarks>
        public UnitTestsHelper() : base(typeof(T).Assembly) {
        }
    }
}
