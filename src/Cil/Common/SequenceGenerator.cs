﻿using System;

namespace Cil.Common {
    /// <summary>
    /// Produces sequences of objects.
    /// </summary>
    /// <typeparam name="T">The type of the objects in the sequence.</typeparam>
    public interface ISequenceGenerator<T> {
        /// <summary>
        /// Get the next object the sequence.
        /// </summary>
        T GetNext();
    }

    /// <summary>
    /// Produces sequences of ints.
    /// </summary>
    public class Int32SequenceGenerator : ISequenceGenerator<int> {
        private readonly int _fromExcluding;

        private readonly bool _cycle;

        private readonly object _chest = new object();

        private int _current;

        /// <summary>
        /// Produces sequences of ints.
        /// </summary>
        /// <param name="fromExcluding">The lowest bound of the sequence (not a part of the sequence).</param>
        /// <param name="cycle">If true, then the sequence returns to its start when exhausted. Otherwise, throws the exception.</param>
        public Int32SequenceGenerator(
            int fromExcluding,
            bool cycle = false
        ) {
            if (fromExcluding == int.MaxValue)
                throw new ArgumentException($"The parameter {nameof(fromExcluding)} " +
                    $"must be less than {nameof(int.MaxValue)} = {int.MaxValue}.");

            this._fromExcluding = fromExcluding;
            this._cycle = cycle;
            this._current = fromExcluding;
        }

        /// <summary>
        /// Get the next number the sequence.
        /// </summary>
        public int GetNext() {
            lock (this._chest) {
                if (this._current == int.MaxValue) {
                    if (this._cycle)
                        this._current = this._fromExcluding + 1;
                    else
                        throw new SequenceExhaustedException();
                } else
                    this._current++;
                return this._current;
            }
        }
    }

    /// <summary>
    /// Produces sequences of longs.
    /// </summary>
    public class Int64SequenceGenerator : ISequenceGenerator<long> {
        private readonly long _fromExcluding;

        private readonly bool _cycle;

        private readonly object _chest = new object();

        private long _current;

        /// <summary>
        /// Produces sequences of longs.
        /// </summary>
        /// <param name="fromExcluding">The lowest bound of the sequence (not a part of the sequence).</param>
        /// <param name="cycle">If true, then the sequence returns to its start when exhausted. Otherwise, throws the exception.</param>
        public Int64SequenceGenerator(
            long fromExcluding,
            bool cycle = false
        ) {
            if (fromExcluding == long.MaxValue)
                throw new ArgumentException($"The parameter {nameof(fromExcluding)} " +
                    $"must be less than {nameof(long.MaxValue)} = {long.MaxValue}.");

            this._fromExcluding = fromExcluding;
            this._cycle = cycle;
            this._current = fromExcluding;
        }

        /// <summary>
        /// Get the next number the sequence.
        /// </summary>
        public long GetNext() {
            lock (this._chest) {
                if (this._current == long.MaxValue) {
                    if (this._cycle)
                        this._current = this._fromExcluding + 1;
                    else
                        throw new SequenceExhaustedException();
                } else
                    this._current++;
                return this._current;
            }
        }
    }
}
