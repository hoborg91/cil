﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Cil.Common {
    /// <summary>
    /// Just two operations: read and write.
    /// </summary>
    [Obsolete("The Cil package has been refactored and renamed to Jail.")]
    public interface IReadWrite<T> {
        T Read();
        void Write(T what);
    }

    // TODO. Are tests necessary for this class?
    /// <summary>
    /// Reads and writes to the console.
    /// </summary>
    [Obsolete("The Cil package has been refactored and renamed to Jail.")]
    public class ConsoleReadWrite : IReadWrite<string> {
        /// <summary>
        /// Reads and writes to the console.
        /// </summary>
        public ConsoleReadWrite() {

        }

        public string Read() {
            return Console.ReadLine();
        }

        public void Write(string what) {
            Console.Write(what);
        }
    }

    /// <summary>
    /// Reads and writes as the consumer specifies.
    /// </summary>
    [Obsolete("The Cil package has been refactored and renamed to Jail.")]
    public class CustomReadWrite<T> : IReadWrite<T> {
        private readonly Func<T> _commandsReader;
        private readonly Action<T> _logger;

        /// <summary>
        /// Reads and writes as the consumer specifies.
        /// </summary>
        /// <param name="commandsReader">Reads from this object.</param>
        /// <param name="logger">Writes to this object.</param>
        public CustomReadWrite(Func<T> commandsReader, Action<T> logger) {
            if (commandsReader == null)
                throw new ArgumentNullException(nameof(commandsReader));
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));

            this._commandsReader = commandsReader;
            this._logger = logger;
        }

        public T Read() {
            return this._commandsReader();
        }

        public void Write(T what) {
            this._logger(what);
        }
    }

    /// <summary>
    /// Writes as consumer specifies. Outputs given values
    /// while reading.
    /// </summary>
    [Obsolete("The Cil package has been refactored and renamed to Jail.")]
    public class PredefinedInputReadWrite<T> : IReadWrite<T> {
        private readonly Action<T> _writer;
        private readonly Queue<T> _input;
        private readonly T _atExhaust;
        private readonly bool _copyToOutput;
        private readonly object _chest = new object();

        /// <summary>
        /// Writes as consumer specifies. Outputs given values
        /// while reading.
        /// </summary>
        /// <param name="writer">Writes to this object.</param>
        /// <param name="atExhaust">Returns this when all input exhausted.</param>
        /// <param name="copyToOutput">If true, writes every given input value to output.</param>
        /// <param name="input">Predefined values to be read one by one.</param>
        public PredefinedInputReadWrite(
            Action<T> writer = null, 
            T atExhaust = default(T), 
            bool copyToOutput = true, 
            params T[] input
        ) {
            this._writer = writer == null
                ? (x) => { }
                : writer;
            this._copyToOutput = copyToOutput;
            this._input = new Queue<T>();
            this._atExhaust = atExhaust;
            if (input == null)
                return;
            foreach (var i in input) {
                this._input.Enqueue(i);
            }
        }

        public T Read() {
            lock (this._chest) {
                var result = this._input.Any()
                    ? this._input.Dequeue()
                    : this._atExhaust;
                this._writer(result);
                return result;
            }
        }

        public void Write(T what) {
            this._writer(what);
        }
    }

    public static class ReadWrite {
        /// <summary>
        /// Returns a Func, returning lines from the given files
        /// one by one. If all lines are exhausted, then returns
        /// nulls.
        /// </summary>
        public static Func<string> GetFileReader(string fileName) {
            return GetFileReader(fileName, new Design.FileSystem.FileSystemApi());
        }

        /// <summary>
        /// Returns a Func, returning lines from the given files
        /// one by one. If all lines are exhausted, then returns
        /// nulls.
        /// </summary>
        public static Func<string> GetFileReader(
            string fileName,
            Design.FileSystem.IFileSystemApi fileSystemApi
        ) {
            string content;
            using (var sr = fileSystemApi.OpenForRead(fileName)) {
                content = sr.ReadToEnd();
            }
            var lines = content
                .Split(
                    new[] { Environment.NewLine, }, 
                    StringSplitOptions.None
                )
                .ToQueue();
            return () => {
                if (lines.Any())
                    return lines.Dequeue();
                return null;
            };
        }
    }
}
