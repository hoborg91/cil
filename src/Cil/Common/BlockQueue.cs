﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cil.Common {
    /// <summary>
    /// One can add values to the IBlockQueue one by one and
    /// read them as arrays. Like moving window.
    /// </summary>
    public interface IBlockQueue<T> {
        /// <summary>
        /// Adds values to the block queue.
        /// </summary>
        /// <param name="values">Values to be added.</param>
        void Push(IEnumerable<T> values);

        /// <summary>
        /// Adds values to the block queue.
        /// </summary>
        /// <param name="values">Values to be added.</param>
        void Push(params T[] values);

        /// <summary>
        /// Returns the next block.
        /// </summary>
        /// <param name="ifNotReady">Behaviour when the block is not ready.</param>
        T[] Pop(IBlockQueueIfNotReady ifNotReady = IBlockQueueIfNotReady.Exception);

        /// <summary>
        /// Returns true, if the first output block is full and can be read. Otherwise, false.
        /// </summary>
        bool Ready();
    }

    /// <summary>
    /// What to do when the block queue is not ready and
    /// someone wants to read the next sequence from it.
    /// </summary>
    public enum IBlockQueueIfNotReady {
        /// <summary>
        /// Return block anyway.
        /// </summary>
        Return,

        /// <summary>
        /// Return null.
        /// </summary>
        Null,

        /// <summary>
        /// Throw BlockIsNoReadyException.
        /// </summary>
        Exception,
    }

    /// <summary>
    /// One can add values to the IBlockQueue one by one and
    /// read them as arrays. Like moving window.
    /// </summary>
    public class BlockQueue<T> : IBlockQueue<T> {
        /// <summary>
        /// The length of the output vectors.
        /// </summary>
        private readonly int _blockSize;

        /// <summary>
        /// The step between included values.
        /// </summary>
        private readonly int _offset;

        /// <summary>
        /// Stores input values.
        /// </summary>
        private readonly Queue<T> _input = new Queue<T>();

        /// <summary>
        /// Stores values to be included in the next output.
        /// </summary>
        private IList<T> _forOutput = new List<T>();

        private readonly object _chest = new object();

        /// <summary>
        /// One can add values to the IBlockQueue one by one and
        /// read them as arrays.
        /// </summary>
        /// <param name="blockSize">Size of the output arrays.</param>
        /// <param name="offset">Offset between input values.</param>
        public BlockQueue(int blockSize, int offset) {
            if (blockSize < 1)
                throw new ArgumentException(nameof(blockSize) + " must be >= 1.");
            if (offset < 1)
                throw new ArgumentException(nameof(offset) + " must be >= 1.");

            this._blockSize = blockSize;
            this._offset = offset;
        }

        /// <summary>
        /// Adds values to the block queue. All given values are added
        /// simultaniously, i. e. if the two threads add their
        /// collections of values in one block queue, then the one of 
        /// the collection will be added completely at first, and then 
        /// the other.
        /// </summary>
        /// <param name="values">Values to be added.</param>
        public void Push(IEnumerable<T> values) {
            if (values == null)
                throw new ArgumentNullException(nameof(values));
            lock (this._chest) {
                foreach (var value in values) {
                    _input.Enqueue(value);
                }
            }
        }

        /// <summary>
        /// Adds values to the block queue. All given values are added
        /// simultaniously, i. e. if the two threads add their
        /// collections of values in one block queue, then the one of 
        /// the collection will be added completely at first, and then 
        /// the other.
        /// </summary>
        /// <param name="values">Values to be added.</param>
        public void Push(params T[] values) {
            this.Push((IEnumerable<T>)values);
        }

        /// <summary>
        /// Returns the next block.
        /// </summary>
        /// <param name="ifNotReady">Behaviour when the block is not ready.</param>
        public T[] Pop(IBlockQueueIfNotReady ifNotReady = IBlockQueueIfNotReady.Exception) {
            lock (this._chest) {
                if (!this._isReady(false))
                    switch (ifNotReady) {
                        case IBlockQueueIfNotReady.Return:
                            break;
                        case IBlockQueueIfNotReady.Null:
                            return null;
                        case IBlockQueueIfNotReady.Exception:
                            throw new BlockIsNoReadyException();
                        default:
                            break;
                    }

                var r = new T[this._blockSize];
                var ri = 0;
                for (; ri < this._forOutput.Count; ri++) {
                    r[ri] = this._forOutput[ri];
                }
                for (; ri < this._blockSize && this._input.Any(); ri++) {
                    r[ri] = this._input.Dequeue();
                }
                this._forOutput = r
                    .Where((x, i) => i >= this._offset)
                    .ToList();
                return r;
            }
        }

        /// <summary>
        /// Returns true, if the first output block is full and can be read.
        /// Otherwise, false.
        /// </summary>
        public bool Ready() {
            return this._isReady(true);
        }

        private bool _isReady(bool issueLock) {
            if (!issueLock)
                return this._forOutput.Count + this._input.Count >= this._blockSize;
            lock (this._chest) {
                return this._forOutput.Count + this._input.Count >= this._blockSize;
            }
        }

        public override string ToString() {
            var result = new StringBuilder("{");
            foreach (var value in this._input) {
                result.Append(value + " ");
            }
            result.Append("} -> (");
            foreach(var value in this._forOutput
                .Union(this._input
                    .Take(this._blockSize - this._forOutput.Count)
                )
            ) {
                result.Append(value.ToString() + " ");
            }
            result.Append(")");
            return result.ToString();
        }
    }
}
