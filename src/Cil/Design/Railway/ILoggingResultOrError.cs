﻿using System;
using System.Collections.Generic;

namespace Cil.Design.Railway {
    /// <summary>
    /// Represents a result of some operation (of the given type) 
    /// or indicates the failure of that operation and provides a 
    /// log of the sub-operations which the main operation consists of.
    /// </summary>
    [Obsolete("The Cil package has been refactored and renamed to Jail.")]
    public interface ILoggingResultOrError<out TResult, TLogEntry> 
        : IResultOrError<TResult> 
    {
        /// <summary>
        /// Contains information about the passed stages of the operation.
        /// </summary>
        IReadOnlyList<TLogEntry> Log { get; }

        /// <summary>
        /// If the current result is successfull then calculates the next 
        /// value using the given value calculator, assignes it to the given variable 
        /// and transfers this value further. If the current result is failed then 
        /// just transfers the failure information further.
        /// </summary>
        ILoggingResultOrError<T, TLogEntry> AssignOnSuccess<T>(
            ref ILoggingResultOrError<T, TLogEntry> modifiedVariable,
            Func<TResult, ILoggingResultOrError<T, TLogEntry>> valueCalculator
        );

        /// <summary>
        /// Implements the specified operation after the successfull result 
        /// of the current operation.
        /// </summary>
        ILoggingResultOrError<T, TLogEntry> OnSuccess<T>(
            Func<TResult, ILoggingResultOrError<T, TLogEntry>> continuation
        );
    }
}
