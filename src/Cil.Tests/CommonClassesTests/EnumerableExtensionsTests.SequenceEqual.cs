﻿using Cil.Common;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CilTests.CommonClassesTests {
    [TestFixture]
    public partial class EnumerableExtensionsTests {
        private static double[][] _jaggedArray_1_1 = new[] { new[] { 0.0, }, };
        private static double[][] _sameJaggedArray_1_1 = new[] { new[] { 0.0, }, };
        private static double[][] _otherJaggedArray_1_1 = new[] { new[] { 0.1, }, };
        private static double[][] _jaggedArray_1_1_null = new[] { (double[])null, };

        private static double[][] _jaggedArray_1_2 = new[] { new[] { 0.0, 0.1, }, };
        private static double[][] _sameJaggedArray_1_2 = new[] { new[] { 0.0, 0.1, }, };
        private static double[][] _otherJaggedArray_1_2 = new[] { new[] { 0.0, -0.1, }, };

        private static double[][] _jaggedArray_2_1 = new[] { new[] { 0.0, }, new[] { 0.0, }, };
        private static double[][] _sameJaggedArray_2_1 = new[] { new[] { 0.0, }, new[] { 0.0, }, };
        private static double[][] _otherJaggedArray_2_1 = new[] { new[] { 0.0, }, new[] { 0.1, }, };
        private static double[][] _jaggedArray_2_1_null_1 = new[] { (double[])null, new[] { 0.0, }, };
        private static double[][] _sameJaggedArray_2_1_null_1 = new[] { (double[])null, new[] { 0.0, }, };
        private static double[][] _jaggedArray_2_1_null_2 = new[] { new[] { 0.0, }, (double[])null, };

        [Test]
        [TestCaseSource(nameof(GetSequenceEqualTestCases))]
        public void Test_SequenceEqual(SequenceEqualTestCase testCase) {
            // Arrange (see the TestCaseSource method).

            // Act
            var result = EnumerableExtensions.SequenceEqual(
                testCase.A,
                testCase.B
            );

            // Assert
            Assert.AreEqual(testCase.ExpectedResult, result);
        }

        [Test]
        public void Test_SequenceEqual_WithComparer_1_1() {
            // Arrange

            // Act
            var result = EnumerableExtensions.SequenceEqual(
                _jaggedArray_1_1,
                _otherJaggedArray_1_1,
                new AlwaysEqualComparer()
            );

            // Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void Test_SequenceEqual_WithComparer_1_2() {
            // Arrange

            // Act
            var result = EnumerableExtensions.SequenceEqual(
                _jaggedArray_1_2,
                _otherJaggedArray_1_2,
                new AlwaysEqualComparer()
            );

            // Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void Test_SequenceEqual_WithComparer_2_1() {
            // Arrange

            // Act
            var result = EnumerableExtensions.SequenceEqual(
                _jaggedArray_2_1,
                _otherJaggedArray_2_1,
                new AlwaysEqualComparer()
            );

            // Assert
            Assert.IsTrue(result);
        }

        private class AlwaysEqualComparer : IEqualityComparer<double> {
            public bool Equals(double x, double y) {
                return true;
            }

            public int GetHashCode(double obj) {
                return 0;
            }
        }

        public static IEnumerable<SequenceEqualTestCase> GetSequenceEqualTestCases() {
            yield return new SequenceEqualTestCase(
                null,
                null,
                true
            );
            yield return new SequenceEqualTestCase(
                _jaggedArray_1_1,
                _sameJaggedArray_1_1,
                true
            );
            yield return new SequenceEqualTestCase(
                _jaggedArray_1_1,
                null,
                false
            );
            yield return new SequenceEqualTestCase(
                null,
                _sameJaggedArray_1_1,
                false
            );

            yield return new SequenceEqualTestCase(
                _jaggedArray_1_1,
                _otherJaggedArray_1_1,
                false
            );
            yield return new SequenceEqualTestCase(
                _jaggedArray_1_1,
                _jaggedArray_1_1_null,
                false
            );
            yield return new SequenceEqualTestCase(
                _jaggedArray_1_1_null,
                _jaggedArray_1_1,
                false
            );

            yield return new SequenceEqualTestCase(
                _jaggedArray_1_1,
                _jaggedArray_1_2,
                false
            );
            yield return new SequenceEqualTestCase(
                _jaggedArray_1_2,
                _jaggedArray_1_1,
                false
            );

            yield return new SequenceEqualTestCase(
                _jaggedArray_1_2,
                _sameJaggedArray_1_2,
                true
            );
            yield return new SequenceEqualTestCase(
                _jaggedArray_1_2,
                _otherJaggedArray_1_2,
                false
            );

            yield return new SequenceEqualTestCase(
                _jaggedArray_1_1,
                _jaggedArray_2_1,
                false
            );
            yield return new SequenceEqualTestCase(
                _jaggedArray_2_1,
                _sameJaggedArray_2_1,
                true
            );
            yield return new SequenceEqualTestCase(
                _jaggedArray_2_1,
                _otherJaggedArray_2_1,
                false
            );
            yield return new SequenceEqualTestCase(
                _jaggedArray_2_1,
                _jaggedArray_2_1_null_1,
                false
            );
            yield return new SequenceEqualTestCase(
                _jaggedArray_2_1,
                _jaggedArray_2_1_null_2,
                false
            );
            yield return new SequenceEqualTestCase(
                _jaggedArray_2_1_null_1,
                _sameJaggedArray_2_1_null_1,
                true
            );
        }

        public class SequenceEqualTestCase {
            public double[][] A { get; }

            public double[][] B { get; }

            public bool ExpectedResult { get; }

            public SequenceEqualTestCase(
                double[][] a,
                double[][] b,
                bool expectedResult
            ) {
                this.A = a;
                this.B = b;
                this.ExpectedResult = expectedResult;
            }

            public override string ToString() {
                var result = new StringBuilder();
                result.Append(this._print(this.A));
                result.Append(" ");
                result.Append(this._print(this.B));
                result.Append(" ");
                result.Append(this.ExpectedResult.ToString());
                return result.ToString();
            }

            private string _print(double[][] array) {
                if (array == null)
                    return "null";
                var result = new StringBuilder("{");
                for (int i = 0; i < array.Length; i++) {
                    var a = array[i];
                    if (a == null) {
                        result.Append("null");
                        continue;
                    }
                    result.Append("{");
                    result.Append(a.Select(x => x.ToString()).JoinBy(", "));
                    result.Append("}");
                }
                result.Append("}");
                return result.ToString();
            }
        }
    }
}
