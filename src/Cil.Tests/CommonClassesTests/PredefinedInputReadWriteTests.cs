﻿using Cil.Common;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Cil.Design.FileSystem;

namespace Cil.Tests.CommonClassesTests {
    [TestFixture]
    public class PredefinedInputReadWriteTests {
        [Test]
        public void Test_CustomReadWrite_Read() {
            // Arrange
            var words = new[] { "a", "b", "c", };
            var wordIndex = -1;
            var supplier = (Func<string>)(() => words[++wordIndex]);
            var rw = new PredefinedInputReadWrite<string>(
                null,
                null,
                false,
                words
            );

            // Act, Assert
            for (int i = 0; i < words.Length; i++) {
                var read = rw.Read();
                Assert.AreEqual(words[i], read);
            }
        }

        [Test]
        public void Test_CustomReadWrite_Read_AtExhaust() {
            // Arrange
            var words = new[] { "a", "b", "c", };
            var atExhaust = "atExhaust";
            var wordIndex = -1;
            var supplier = (Func<string>)(() => words[++wordIndex]);
            var rw = new PredefinedInputReadWrite<string>(
                null,
                atExhaust,
                false,
                words
            );

            // Act, Assert
            for (int i = 0; i < words.Length; i++) {
                var read = rw.Read();
                Assert.AreEqual(words[i], read);
            }
            for (int i = 0; i < words.Length; i++) {
                Assert.AreEqual(atExhaust, rw.Read());
            }
        }

        [Test]
        public void Test_CustomReadWrite_Read_CopyToOutput() {
            // Arrange
            var words = new[] { "a", "b", "c", };
            var wordIndex = -1;
            var supplier = (Func<string>)(() => words[++wordIndex]);
            var fromWrite = new List<string>();
            var writer = (Action<string>)(x => fromWrite.Add(x));
            var rw = new PredefinedInputReadWrite<string>(
                writer,
                null,
                true,
                words
            );

            // Act, Assert
            for (int i = 0; i < words.Length; i++) {
                var read = rw.Read();
                Assert.AreEqual(words[i], read);
            }
            CollectionAssert.AreEqual(words, fromWrite);
        }

        [Test]
        public void Test_PredefinedInputReadWrite_Write() {
            // Arrange
            var expected = new[] { "a", "b", "c", };
            var written = new List<string>();
            var writer = (Action<string>)(x => written.Add(x));
            var rw = new PredefinedInputReadWrite<string>(
                writer
            );

            // Act
            for (int i = 0; i < expected.Length; i++) {
                rw.Write(expected[i]);
            }

            // Assert
            CollectionAssert.AreEqual(expected, written);
        }

        [Test]
        public void Test_GetFileReader() {
            // Arrange
            var fileName = "";
            var expected = new[] {
                "A",
                "B",
                "C",
            };
            var fileContent = string.Join(
                Environment.NewLine,
                expected
            );
            var fs = new FileSystemForTests(
                fileName,
                fileContent
            );
            var fr = ReadWrite.GetFileReader(
                fileName,
                fs
            );

            // Act
            var fromGetReader = new List<string>();
            string line;
            while ((line = fr()) != null) {
                fromGetReader.Add(line);
            }

            // Assert
            CollectionAssert.AreEqual(expected, fromGetReader);
        }

        // TODO. When some mocking framework will be used,
        // replace this file with that mocks. Or create public
        // custom implementation for tests (in the main assembly).
        private class FileSystemForTests : IFileSystemApi {
            private readonly Dictionary<string, IStreamReader> _readers =
                new Dictionary<string, IStreamReader>();

            public FileSystemForTests(
                string fileName,
                string fileContent
            ) {
                this._readers[fileName] = new StreamReaderForTests(fileContent);
            }

            public void DeleteFile(string path) {
                throw new NotImplementedException();
            }

            public bool DirectoryExists(string path) {
                throw new NotImplementedException();
            }

            public bool FileExists(string path) {
                throw new NotImplementedException();
            }

            public string GetDirectoryName(string path) {
                throw new NotImplementedException();
            }

            public string[] GetFiles(string path) {
                throw new NotImplementedException();
            }

            public IStreamReader OpenForRead(string path) {
                if (this._readers.ContainsKey(path))
                    return this._readers[path];
                throw new System.IO.FileNotFoundException();
            }

            public IStreamWriter OpenForWrite(string path) {
                throw new NotImplementedException();
            }

            public string ReadAllText(string path) {
                throw new NotImplementedException();
            }

            public string ReadAllText(string path, Encoding encoding) {
                throw new NotImplementedException();
            }

            public void WriteAllLines(string path, IEnumerable<string> contents) {
                throw new NotImplementedException();
            }

            public void WriteAllLines(string path, IEnumerable<string> contents, Encoding encoding) {
                throw new NotImplementedException();
            }

            public void WriteAllText(string path, string contents) {
                throw new NotImplementedException();
            }

            public void WriteAllText(string path, string contents, Encoding encoding) {
                throw new NotImplementedException();
            }

            private class StreamReaderForTests : IStreamReader {
                private readonly string _content;

                public StreamReaderForTests(string content) {
                    this._content = content;
                }

                public void Dispose() {
                    
                }

                public string ReadLine() {
                    throw new NotImplementedException();
                }

                public string ReadToEnd() {
                    return this._content;
                }
            }
        }
    }
}
