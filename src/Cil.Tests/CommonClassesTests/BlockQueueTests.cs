﻿using System.Collections.Generic;
using Cil.Common;
using System;
using NUnit.Framework;

namespace CilTests.CommonClassesTests {
    /// <summary>
    /// Contains tests for BlockQueue.
    /// </summary>
    [TestFixture]
    public class BlockQueueTests {
        /// <summary>
        /// Feeds a BlockQueue(3, 1) with sequence (0, 1, 2, 3, 4).
        /// Checks that the output of the block queue consists of
        /// and only of (0, 1, 2), (1, 2, 3) and (2, 3, 4).
        /// </summary>
        [Test]
        public void BlockQueueTest1() {
            // Arrange
            var blockSize = 3;

            // Act
            IBlockQueue<int> bq = new BlockQueue<int>(blockSize, 1);
            var results = _populateBlockQueue(bq);

            // Assert
            Assert.AreEqual(results.Count, 3);
            for (int i = 0; i < results.Count; i++) {
                Assert.AreEqual(results[i].Length, blockSize);
                for (int j = 0; j < blockSize; j++) {
                    Assert.AreEqual(results[i][j], i + j);
                }
            }
        }

        /// <summary>
        /// Feeds a BlockQueue(2, 2) with sequence (0, 1, 2, 3, 4).
        /// Checks that the output of the block queue consists of
        /// and only of (0, 1) and (2, 3).
        /// </summary>
        [Test]
        public void BlockQueueTest2() {
            // Arrange
            var blockSize = 3;

            // Act
            IBlockQueue<int> bq = new BlockQueue<int>(blockSize, 2);
            var results = _populateBlockQueue(bq);

            // Assert
            Assert.AreEqual(results.Count, 2);
            for (int i = 0; i < results.Count; i++) {
                Assert.AreEqual(results[i].Length, blockSize);
                for (int j = 0; j < blockSize; j++) {
                    Assert.AreEqual(results[i][j], i * 2 + j);
                }
            }
        }

        [Test]
        public void BlockQueue_CtorWrongArgument_BlockSize() {
            // Arrange, Act, Assert (+ in method attribute)
            Assert.Throws<ArgumentException>(() => {
                var bq = new BlockQueue<int>(0, 1);
            });
        }

        [Test]
        public void BlockQueue_CtorWrongArgument_Offset() {
            // Arrange, Act, Assert (+ in method attribute)
            Assert.Throws<ArgumentException>(() => {
                var bq = new BlockQueue<int>(1, 0);
            });
        }

        [Test]
        public void BlockQueue_PushWrongArgument_ValuesArray() {
            // Arrange
            var bq = new BlockQueue<int>(1, 1);

            // Act, Assert (+ in method attribute)
            Assert.Throws<ArgumentNullException>(() => {
                bq.Push((int[])null);
            });
        }

        [Test]
        public void BlockQueue_PushWrongArgument_ValuesIEnumerable() {
            // Arrange
            var bq = new BlockQueue<int>(1, 1);

            // Act, Assert (+ in method attribute)
            Assert.Throws<ArgumentNullException>(() => {
                bq.Push((IEnumerable<int>)null);
            });
        }

        [Test]
        public void BlockQueue_PopWhenNotReady_WithException() {
            // Arrange
            var bq = new BlockQueue<int>(1, 1);

            // Act, Assert (+ in method attribute)
            Assert.Throws<BlockIsNoReadyException>(() => {
                bq.Pop(IBlockQueueIfNotReady.Exception);
            });
        }

        [Test]
        public void BlockQueue_PopWhenNotReady_ReturnAnyway() {
            // Arrange
            var bq = new BlockQueue<int>(1, 1);
            
            // Act
            var block = bq.Pop(IBlockQueueIfNotReady.Return);

            // Assert
            Assert.IsNotNull(block);
            Assert.AreEqual(1, block.Length);
            Assert.AreEqual(default(int), block[0]);
        }

        [Test]
        public void BlockQueue_PopWhenNotReady_ReturnNull() {
            // Arrange
            var bq = new BlockQueue<int>(1, 1);

            // Act
            var block = bq.Pop(IBlockQueueIfNotReady.Null);

            // Assert
            Assert.IsNull(block);
        }

        /// <summary>
        /// Populates the given block queue with the given 
        /// amount of values, reads the output of the block 
        /// queue to the List and returns that list.
        /// </summary>
        private List<int[]> _populateBlockQueue(
            IBlockQueue<int> blockQueue, 
            int count = 5,
            IBlockQueueIfNotReady ifNotReady = IBlockQueueIfNotReady.Exception
        ) {
            for (int i = 0; i < count; i++) {
                blockQueue.Push(i);
            }
            var results = new List<int[]>();
            while (blockQueue.Ready()) {
                results.Add(blockQueue.Pop(ifNotReady));
            }
            return results;
        }
    }
}
