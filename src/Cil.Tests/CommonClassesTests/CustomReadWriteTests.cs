﻿using Cil.Common;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Cil.Tests.CommonClassesTests {
    [TestFixture]
    public class CustomReadWriteTests {
        [Test]
        public void Test_CustomReadWrite_Ctor_WrongArgument_Null1() {
            // Arrange, Act, Assert
            Assert.Throws<ArgumentNullException>(() => {
                new CustomReadWrite<string>(
                    null,
                    x => { }
                );
            });
        }

        [Test]
        public void Test_CustomReadWrite_Ctor_WrongArgument_Null2() {
            // Arrange, Act, Assert
            Assert.Throws<ArgumentNullException>(() => {
                new CustomReadWrite<string>(
                    () => string.Empty,
                    null
                );
            });
        }

        [Test]
        public void Test_CustomReadWrite_Read() {
            // Arrange
            var words = new[] { "a", "b", "c", };
            var wordIndex = -1;
            var supplier = (Func<string>)(() => words[++wordIndex]);
            var rw = new CustomReadWrite<string>(
                supplier,
                x => { }
            );

            // Act, Assert
            for (int i = 0; i < words.Length; i++) {
                var read = rw.Read();
                Assert.AreEqual(words[i], read);
            }
        }

        [Test]
        public void Test_CustomReadWrite_Write() {
            // Arrange
            var expected = new[] { "a", "b", "c", };
            var written = new List<string>();
            var writer = (Action<string>)(x => written.Add(x));
            var rw = new CustomReadWrite<string>(
                () => string.Empty,
                writer
            );

            // Act
            for (int i = 0; i < expected.Length; i++) {
                rw.Write(expected[i]);
            }

            // Assert
            CollectionAssert.AreEqual(expected, written);
        }
    }
}
