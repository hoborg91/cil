﻿using Cil.Common;
using NUnit.Framework;
using System;

namespace Cil.Tests.CommonClassesTests {
    [TestFixture]
    public class Int32SequenceGeneratorTests {
        [Test]
        public void Ctor_ThrowsOnWrongArgument() {
            Assert.Throws<ArgumentException>(() => {
                new Int32SequenceGenerator(int.MaxValue);
            });
        }

        [Test]
        public void GetNext() {
            // Arrange
            int fromExcluding = 0;
            var sut = new Int32SequenceGenerator(
                fromExcluding
            );

            // Act
            var next = sut.GetNext();

            // Assert
            Assert.AreEqual(fromExcluding + 1, next);
        }

        [Test]
        public void GetNext_OnCycle() {
            // Arrange
            var fromExcluding = int.MaxValue - 1;
            var sut = new Int32SequenceGenerator(
                fromExcluding,
                true
            );

            // Act
            var next1 = sut.GetNext();
            var next2 = sut.GetNext();

            // Assert
            Assert.AreEqual(fromExcluding + 1, next1);
            Assert.AreEqual(fromExcluding + 1, next2);
        }
    }

    [TestFixture]
    public class Int64SequenceGeneratorTests {
        [Test]
        public void Ctor_ThrowsOnWrongArgument() {
            Assert.Throws<ArgumentException>(() => {
                new Int64SequenceGenerator(long.MaxValue);
            });
        }

        [Test]
        public void GetNext() {
            // Arrange
            long fromExcluding = 0;
            var sut = new Int64SequenceGenerator(
                fromExcluding
            );

            // Act
            var next = sut.GetNext();

            // Assert
            Assert.AreEqual(fromExcluding + 1, next);
        }

        [Test]
        public void GetNext_OnCycle() {
            // Arrange
            var fromExcluding = long.MaxValue - 1;
            var sut = new Int64SequenceGenerator(
                fromExcluding,
                true
            );

            // Act
            var next1 = sut.GetNext();
            var next2 = sut.GetNext();

            // Assert
            Assert.AreEqual(fromExcluding + 1, next1);
            Assert.AreEqual(fromExcluding + 1, next2);
        }
    }
}
