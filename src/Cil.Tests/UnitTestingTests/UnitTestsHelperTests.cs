﻿using Cil.UnitTesting;
using Moq;
using NUnit.Framework;
using System;
using System.Reflection;

namespace Cil.Tests.UnitTestingTests {
    [TestFixture]
    public class UnitTestsHelperTests {
        private Mock<ITypesFinder> _typesFinderMock;

        private Mock<IReflectionHelper> _reflectionHelperMock;

        private UnitTestsHelper _getSut(
            ITypesFinder typesFinder,
            IReflectionHelper reflectionHelper
        ) {
            return new UnitTestsHelper(
                typesFinder,
                reflectionHelper
            );
        }

        [SetUp]
        public void SetUp() {
            this._typesFinderMock = new Mock<ITypesFinder>();
            this._reflectionHelperMock = new Mock<IReflectionHelper>();
        }

        [Test]
        public void Test_New() {
            // Arrange
            var (sut, typeName, constructorArguments) =
                this._prepareCommonTestCase();

            // Act
            sut.New<IStub>(typeName, constructorArguments);

            // Assert
            this._verifyTypesFinder(typeName);
            this._reflectionHelperMock.Verify(x => x.New<IStub>(
                It.IsAny<Type>(),
                constructorArguments), Times.Once);
        }

        [Test]
        public void Test_TestConstructorForNullArgumentsCheck() {
            // Arrange
            var (sut, typeName, constructorArguments) =
                this._prepareCommonTestCase();

            // Act
            sut.TestConstructorForNullArgumentsCheck(
                typeName,
                constructorArguments
            );

            // Assert
            this._verifyTypesFinder(typeName);
            this._reflectionHelperMock.Verify(x => x.TestConstructorForNullArgumentsCheck(
                It.IsAny<Type>(),
                constructorArguments), Times.Once);
        }

        [Test]
        public void Test_TestConstructorForNullArgumentsCheck_WithExpression() {
            // Arrange
            var typesFinderMock = new Mock<ITypesFinder>();
            var reflectionHelperMock = new Mock<IReflectionHelper>();
            var sut = this._getSut(
                typesFinderMock.Object,
                reflectionHelperMock.Object
            );
            var assembly = Assembly.GetExecutingAssembly();

            // Act
            sut.TestConstructorForNullArgumentsCheck(() => new UnitTestsHelper(
                assembly
            ));

            // Assert
            typesFinderMock.Verify(
                tf => tf.FindType(It.IsAny<string>(), It.IsAny<string>()),
                Times.Never
            );
            reflectionHelperMock.Verify(
                rh => rh.TestConstructorForNullArgumentsCheck(
                    typeof(UnitTestsHelper),
                    assembly
                ), 
                Times.Once
            );
        }

        private void _verifyTypesFinder(string typeName) {
            this._typesFinderMock.Verify(
                x => x.FindType(
                    typeName,
                    null
                ), 
                Times.Once
            );
        }

        private (UnitTestsHelper, string, object[]) _prepareCommonTestCase() {
            var sut = this._getSut(
                this._typesFinderMock.Object,
                this._reflectionHelperMock.Object
            );
            var typeName = "Type";
            var constructorArguments = new object[] { new object(), };
            return (sut, typeName, constructorArguments);
        }

        private interface IStub { }
    }
}
