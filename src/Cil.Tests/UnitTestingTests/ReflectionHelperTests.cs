﻿using Cil.Design;
using Cil.UnitTesting;
using NUnit.Framework;
using System;

namespace Cil.Tests.UnitTestingTests {
    [TestFixture]
    public class ReflectionHelperTests {
        private ReflectionHelper _sut;

        private object[] _constructorArguments = new object[] {
            new A(),
            new B(),
            1,
        };

        [SetUp]
        public void SetUp() {
            this._sut = new ReflectionHelper();
        }

        #region Without attributes

        [Test]
        [TestCase(typeof(CtorAllChecks))]
        [TestCase(typeof(CtorAllChecksMyA))]
        [TestCase(typeof(CtorAllChecksMyAll))]
        [TestCase(typeof(CtorAllChecksCanBeNullB))]
        [TestCase(typeof(CtorAllChecksCanBeNullAll))]
        public void TestConstructorForNullArgumentsCheck_AllChecks_NUnit(
            Type type
        ) {
            // Arrange, Act, Assert
            // The test checks that the code does not throw.
            // Additional assert statement is not necessary.
            this._sut.TestConstructorForNullArgumentsCheck(
                type,
                a => Assert.Multiple(() => a()),
                (a, p) => Assert.Throws<ArgumentNullException>(() => a(), p.Name),
                this._constructorArguments
            );
        }

        [Test]
        [TestCase(typeof(CtorAllChecks))]
        [TestCase(typeof(CtorAllChecksMyA))]
        [TestCase(typeof(CtorAllChecksMyAll))]
        [TestCase(typeof(CtorAllChecksCanBeNullB))]
        [TestCase(typeof(CtorAllChecksCanBeNullAll))]
        public void TestConstructorForNullArgumentsCheck_AllChecks_Custom(
            Type type
        ) {
            // Arrange, Act, Assert
            // The test checks that the code does not throw.
            // Additional assert statement is not necessary.
            this._sut.TestConstructorForNullArgumentsCheck(
                type,
                this._constructorArguments
            );
        }

        [Test]
        [TestCase(typeof(CtorNoChecks))]
        [TestCase(typeof(CtorNoChecksMyA))]
        [TestCase(typeof(CtorNoChecksCanBeNullB))]
        [TestCase(typeof(CtorNoChecksCanBeNullAll))]
        [TestCase(typeof(CtorCheckA))]
        [TestCase(typeof(CtorCheckB))]
        public void TestConstructorForNullArgumentsCheck_NoChecks_NUnit(
            Type type
        ) {
            // Arrange, Act, Assert
            Assert.Throws<MultipleAssertException>(() => {
                this._sut.TestConstructorForNullArgumentsCheck(
                    type,
                    a => Assert.Multiple(() => a()),
                    (a, p) => Assert.Throws<ArgumentNullException>(() => a(), p.Name),
                    this._constructorArguments
                );
            });
        }

        [Test]
        [TestCase(typeof(CtorNoChecks))]
        [TestCase(typeof(CtorNoChecksMyA))]
        [TestCase(typeof(CtorNoChecksCanBeNullB))]
        [TestCase(typeof(CtorNoChecksCanBeNullAll))]
        public void TestConstructorForNullArgumentsCheck_NoChecks_Custom(
            Type type
        ) {
            // Arrange, Act, Assert
            Assert.Throws<AggregateException>(() => {
                this._sut.TestConstructorForNullArgumentsCheck(
                    type,
                    this._constructorArguments
                );
            });
        }

        #endregion Without attributes

        #region With attributes

        [Test]
        [TestCase(typeof(CtorAllChecks))]
        [TestCase(typeof(CtorAllChecksMyA))]
        [TestCase(typeof(CtorAllChecksMyAll))]
        [TestCase(typeof(CtorAllChecksCanBeNullB))]
        [TestCase(typeof(CtorAllChecksCanBeNullAll))]
        public void TestConstructorForNullArgumentsCheck_WithCustomAttribute_NUnit_Positive(
            Type type
        ) {
            // Arrange, Act, Assert
            // The test checks that the code does not throw.
            // Additional assert statement is not necessary.
            this._sut.TestConstructorForNullArgumentsCheckExceptAttributes<MyAttribute>(
                type,
                a => Assert.Multiple(() => a()),
                (a, p) => Assert.Throws<ArgumentNullException>(() => a(), p.Name),
                this._constructorArguments
            );
        }

        [Test]
        [TestCase(typeof(CtorNoChecksMyA))]
        [TestCase(typeof(CtorCheckA))]
        [TestCase(typeof(CtorCheckB))]
        public void TestConstructorForNullArgumentsCheck_WithCustomAttribute_NUnit_Negative(
            Type type
        ) {
            // Arrange, Act, Assert
            Assert.Throws<NUnit.Framework.MultipleAssertException>(() =>
                this._sut.TestConstructorForNullArgumentsCheckExceptAttributes<MyAttribute>(
                    type,
                    a => Assert.Multiple(() => a()),
                    (a, p) => Assert.Throws<ArgumentNullException>(() => a(), p.Name),
                    this._constructorArguments
                )
            );
        }

        [Test]
        [TestCase(typeof(CtorAllChecks))]
        [TestCase(typeof(CtorAllChecksMyA))]
        [TestCase(typeof(CtorAllChecksMyAll))]
        [TestCase(typeof(CtorAllChecksCanBeNullB))]
        [TestCase(typeof(CtorAllChecksCanBeNullAll))]
        public void TestConstructorForNullArgumentsCheck_WithCustomAttribute_Custom_Positive(
            Type type
        ) {
            // Arrange, Act, Assert
            // The test checks that the code does not throw.
            // Additional assert statement is not necessary.
            this._sut.TestConstructorForNullArgumentsCheckExceptAttributes<MyAttribute>(
                type,
                this._constructorArguments
            );
        }

        [Test]
        [TestCase(typeof(CtorNoChecksMyA))]
        [TestCase(typeof(CtorCheckA))]
        [TestCase(typeof(CtorCheckB))]
        public void TestConstructorForNullArgumentsCheck_WithCustomAttribute_Custom_Negative(
            Type type
        ) {
            // Arrange, Act, Assert
            Assert.Throws<MissingExceptionException>(() =>
                this._sut.TestConstructorForNullArgumentsCheckExceptAttributes<MyAttribute>(
                    type,
                    this._constructorArguments
                )
            );
        }

        [Test]
        [TestCase(typeof(CtorNoChecks))]
        [TestCase(typeof(CtorNoChecksMyA))]
        [TestCase(typeof(CtorNoChecksCanBeNullB))]
        [TestCase(typeof(CtorCheckA))]
        [TestCase(typeof(CtorCheckB))]
        public void TestConstructorForNullArgumentsCheck_CanBeNull_NUnit(
            Type type
        ) {
            // Arrange, Act, Assert
            Assert.Throws<MultipleAssertException>(() => {
                this._sut.TestConstructorForNullArgumentsCheckExceptCanBeNulls(
                    type,
                    a => Assert.Multiple(() => a()),
                    (a, p) => Assert.Throws<ArgumentNullException>(() => a(), p.Name),
                    this._constructorArguments
                );
            });
        }

        [Test]
        [TestCase(typeof(CtorAllChecks))]
        [TestCase(typeof(CtorAllChecksCanBeNullB))]
        [TestCase(typeof(CtorNoChecksCanBeNullAll))]
        public void TestConstructorForNullArgumentsCheck_CanBeNulls_Custom(
            Type type
        ) {
            // Arrange, Act, Assert
            // The test checks that the code does not throw.
            // Additional assert statement is not necessary.
            this._sut.TestConstructorForNullArgumentsCheckExceptCanBeNulls(
                type,
                this._constructorArguments
            );
        }

        #endregion With attributes

        #region Test infrastructure

        private class MyAttribute : Attribute { }

        private class A { }

        private class B { }

        private class CtorNoChecks {
            public CtorNoChecks(A a, B b, int i) {
                
            }
        }

        private class CtorCheckA {
            public CtorCheckA(A a, B b, int i) {
                if (a == null)
                    throw new ArgumentNullException(nameof(a));
            }
        }

        private class CtorCheckB {
            public CtorCheckB(A a, B b, int i) {
                if (b == null)
                    throw new ArgumentNullException(nameof(b));
            }
        }

        private class CtorAllChecks {
            public CtorAllChecks(A a, B b, int i) {
                if (a == null)
                    throw new ArgumentNullException(nameof(a));
                if (b == null)
                    throw new ArgumentNullException(nameof(b));
            }
        }

        private class CtorNoChecksMyA {
            public CtorNoChecksMyA([My]A a, B b, int i) {
                
            }
        }

        private class CtorAllChecksMyA {
            public CtorAllChecksMyA([My]A a, B b, int i) {
                if (a == null)
                    throw new ArgumentNullException(nameof(a));
                if (b == null)
                    throw new ArgumentNullException(nameof(b));
            }
        }

        private class CtorAllChecksMyAll {
            public CtorAllChecksMyAll([My]A a, [My]B b, int i) {
                if (a == null)
                    throw new ArgumentNullException(nameof(a));
                if (b == null)
                    throw new ArgumentNullException(nameof(b));
            }
        }

        private class CtorNoChecksCanBeNullB {
            public CtorNoChecksCanBeNullB(A a, [CanBeNull]B b, int i) {
                
            }
        }

        private class CtorAllChecksCanBeNullB {
            public CtorAllChecksCanBeNullB(A a, [CanBeNull]B b, int i) {
                if (a == null)
                    throw new ArgumentNullException(nameof(a));
                if (b == null)
                    throw new ArgumentNullException(nameof(b));
            }
        }

        private class CtorNoChecksCanBeNullAll {
            public CtorNoChecksCanBeNullAll([CanBeNull]A a, [CanBeNull]B b, int i) {
                
            }
        }

        private class CtorAllChecksCanBeNullAll {
            public CtorAllChecksCanBeNullAll([CanBeNull]A a, [CanBeNull]B b, int i) {
                if (a == null)
                    throw new ArgumentNullException(nameof(a));
                if (b == null)
                    throw new ArgumentNullException(nameof(b));
            }
        }

        #endregion Test infrastructure
    }
}
