![appveyor](https://ci.appveyor.com/api/projects/status/jxrb6cvv23gwqj88?svg=true)

**The project (and the corresponding NuGet Package) has been renamed and moved to [GitHub](https://github.com/hoborg91/jail).**

This is a **C#** project containing some infrastructure classes and extension methods I use frequently in my works. I have decided to place it in a remote repository and make a **NuGet** package to be able to include it to every new project I need. Do not suppose that someone else might find this usefull. But if yes, then use it like you want and like **MIT license** suggest.

How to install: [Install-Package Cil](https://www.nuget.org/packages/Cil/) .
